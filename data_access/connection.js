var massive = require('massive');
var connectionString = 'postgres://postgres:081011@127.0.0.1:5432/swebodb';

var Connetion = function () {	
	var db = massive.connectSync({connectionString : connectionString});
	
	this.find = function (schema, table, pk) {
		var promise = new Promise(function (resolve, reject) {
			db[schema][table].find(pk, function (err, result) {				
				if(err) reject(err);
				else resolve(result);
			});
		});

		return promise;
	}

	this.runQuery = function (query) {
		var p = new Promise(function (resolve, reject) {
			this.client.query(query, function (err, result) {
				if (err) reject(err);
				else resolve(result.rows);
			});
		});

		return p;
	}

	return this;	
}

module.exports = Connetion;