var connection = require('../data_access/connection.js')();

var CountryService = function () {	
	this.schema = 'location';
	this.table = 'country';	

	this.find = function (pk) {		
		return connection.find(this.schema, this.table, pk);
	}

	return this;
}

module.exports.CountryService = CountryService;