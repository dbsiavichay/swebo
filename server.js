var express = require('express'),
	bodyParser = require('body-parser'),		
	swig = require('swig'),
	massive = require('massive');
	//location = require('./services/location.js');

var server = express();
var io = require('socket.io')(server.listen(process.env.PORT || 5000));
//var countryService = new location.CountryService();

//Configuracion de vistas y archivos estaticos
server.engine('html', swig.renderFile);
server.set('view engine', 'html');
server.set('views', './views');
server.set('view cache', false);

server.use(express.static('./static'));
//server.use(session({secret: 'abcd@1234', resave: false, saveUninitialized: true}));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));


server.get('/', function (req, res) {
	res.render('home');
});

server.get('/countries', function (req, res) {
	res.render('countries');
});


io.on('connection', function (socket) {
	//socket.emit('livecode', currentData);
	socket.on('livecode', function (data) {
		if(data===null) {
			sockets1.push(socket);
			socket.emit('livecode', currentData);
			return;
		}

		for(attr in data){
			if(attr === 'html') currentData.html = data.html;
			if(attr === 'css') currentData.css = data.css;
			if(attr === 'js') currentData.js = data.js;
		}

		//socket.broadcast.emit('livecode', data);
		sockets1.forEach(function (sock) {
			if(sock.id!=socket.id) {
				sock.emit('livecode', data);
			}
		});
	});

	socket.on('chat', function (data) {
		socket.broadcast.emit('chat', data);
	});
});

console.log('Servidor escuchando en http://localhost:5000');
